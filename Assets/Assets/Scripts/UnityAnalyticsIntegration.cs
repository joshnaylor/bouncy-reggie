﻿using UnityEngine;
using System.Collections;
using UnityEngine.Cloud.Analytics;

public class UnityAnalyticsIntegration : MonoBehaviour {
	
	// Use this for initialization
	void Start () {
		
		const string appId = "7519601118633022472";
		UnityAnalytics.StartSDK (appId);
		
	}
	
}