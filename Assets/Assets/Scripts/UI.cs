﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
#if UNITY_ANDROID
using UnityEngine.Advertisements;
#endif




public class UI : MonoBehaviour {

	public Text Time;
	public Text Score;
	bool shownAd;
	GameObject Player;
	public int timeSeconds = 30;
	float currentTopScore = 0;
	// Use this for initialization
	void Start () {
		Time.text = "00:" + timeSeconds.ToString ();
		Player = GameObject.Find("Monster");
		InvokeRepeating ("SortTime", 1.0f, 1.0f);

		#if UNITY_ANDROID
		Advertisement.Initialize ("20087", true);
		#endif
	}
	
	// Update is called once per frame
	void Update () {

		SortScore ();
		#if UNITY_ANDROID

		if(shownAd)
		{
			bool showing = Advertisement.isShowing;
			if(showing == false)
			{
				CancelInvoke ("SortTime");
				shownAd = false;
				Application.LoadLevel(0);
			}
		}
		#endif
	}

	void SortScore()
	{
		float currentPlayerHeight = Player.transform.position.y;
		
		if (currentPlayerHeight > currentTopScore)
			currentTopScore = currentPlayerHeight;
		
		currentTopScore = Mathf.RoundToInt(currentTopScore);
		Score.text = currentTopScore.ToString ();
	}

	void SortTime()
	{
		if (--timeSeconds == 0)
		{
			#if UNITY_ANDROID
			//CancelInvoke ("SortTime");
			if(Advertisement.isReady())
			{
				Advertisement.Show();
				shownAd = true;
			}
			#endif

			#if !UNITY_ANDROID
			Application.LoadLevel(0);
			#endif
		}
		Time.text = "00:" + timeSeconds.ToString ();
	}

}
