﻿using UnityEngine;
using System.Collections;
#if UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

public class playerControl : MonoBehaviour {

	GameObject[] greenPlatform;
	GameObject[] redPlatform;
	float jumpHeight = 650f;
	float speed = 5;
	// Use this for initialization
	void Start () {

		greenPlatform = GameObject.FindGameObjectsWithTag("platformGreen");
		redPlatform = GameObject.FindGameObjectsWithTag("platformRed");
		this.gameObject.GetComponent<SpriteRenderer> ().color = Color.green;
	}

	// Update is called once per frame
	void Update () {
	
		#if UNITY_ANDROID
		if (Advertisement.isShowing == true) 
		{
			Time.timeScale = 0;
		} 
		else 
		{
		#endif
			Time.timeScale = 1;
			if (Input.GetKeyDown (KeyCode.Space) || Input.GetMouseButtonDown (0)) {
					this.gameObject.rigidbody2D.AddForce (new Vector2 (0, 300));
			}

			if (Input.GetKey (KeyCode.LeftArrow) || Input.acceleration.x < -0.2) {
					this.gameObject.rigidbody2D.AddForce (new Vector2 (-Input.acceleration.x * Time.deltaTime, 0));
					this.gameObject.rigidbody2D.AddForce (new Vector2 (-20, 0));
					//this.gameObject.rigidbody2D.velocity = new Vector2(-speed, gameObject.rigidbody2D.velocity.y);
			}
			if (Input.GetKey (KeyCode.RightArrow) || Input.acceleration.x > 0.2) {
					this.gameObject.rigidbody2D.AddForce (new Vector2 (Input.acceleration.x * Time.deltaTime, 0));
					this.gameObject.rigidbody2D.AddForce (new Vector2 (20, 0));
					//this.gameObject.rigidbody2D.velocity = new Vector2(speed, gameObject.rigidbody2D.velocity.y);
			}


			if (this.gameObject.GetComponent<SpriteRenderer> ().color == Color.green) {
					if (this.gameObject.rigidbody2D.velocity.y < 1) { //down
							foreach (GameObject greenP in greenPlatform)
									greenP.collider2D.enabled = false;
							foreach (GameObject redP in redPlatform)
									redP.collider2D.enabled = true;
					} else { 											//up
							foreach (GameObject redP in redPlatform)
									redP.collider2D.enabled = true;
							foreach (GameObject greenP in greenPlatform)
									greenP.collider2D.enabled = false;
					}
			} else if (this.gameObject.GetComponent<SpriteRenderer> ().color == Color.red) {
					if (this.gameObject.rigidbody2D.velocity.y < 1) {
							foreach (GameObject greenP in greenPlatform)
									greenP.collider2D.enabled = true;
							foreach (GameObject redP in redPlatform)
									redP.collider2D.enabled = false;
					} else {
							foreach (GameObject greenP in greenPlatform)
									greenP.collider2D.enabled = true;
							foreach (GameObject redP in redPlatform)
									redP.collider2D.enabled = false;
					}
			}
		#if UNITY_ANDROID
		}
		#endif
	}

	void OnTriggerEnter2D (Collider2D col)
	{
		if(col.gameObject.name == "platformGreen")
		{
			foreach(GameObject greenP in greenPlatform)
			{
				if(greenP.collider2D.enabled == true && this.gameObject.GetComponent<SpriteRenderer>().color != Color.green)
				{
					//Debug.Log("collide with green");
					this.gameObject.GetComponent<SpriteRenderer>().color = Color.green;
					if (this.gameObject.rigidbody2D.velocity.y < 1) 
					{
						this.gameObject.rigidbody2D.AddForce(new Vector2(0,jumpHeight));
					}
				}
				else
				{
					//Debug.Log("do nothing on green");
				}
			}
		}
		
		if(col.gameObject.name == "platformRed")
		{
			foreach(GameObject redP in redPlatform)
			{
				if(redP.collider2D.enabled == true && this.gameObject.GetComponent<SpriteRenderer>().color != Color.red)
				{
					//Debug.Log("collide with red");
					this.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
					if (this.gameObject.rigidbody2D.velocity.y < 1) 
					{
						this.gameObject.rigidbody2D.AddForce(new Vector2(0,jumpHeight));
					}
				}
			}
		}

		if(col.gameObject.name == "platformStart")
		{
			//Debug.Log("collide with white");
			if (this.gameObject.rigidbody2D.velocity.y < 1) 
			{
				this.gameObject.rigidbody2D.AddForce(new Vector2(0,jumpHeight));
			}
		}
	}
}
